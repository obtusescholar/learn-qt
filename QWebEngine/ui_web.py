# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_web.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from PySide2.QtWebEngineWidgets import QWebEngineView



class Ui_Web(object):
    def setupUi(self, Web):
        if not Web.objectName():
            Web.setObjectName(u"Web")
        Web.resize(900, 640)
        self.verticalLayout = QVBoxLayout(Web)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.lineEditUrl = QLineEdit(Web)
        self.lineEditUrl.setObjectName(u"lineEditUrl")

        self.horizontalLayout.addWidget(self.lineEditUrl)

        self.pushButtonGo = QPushButton(Web)
        self.pushButtonGo.setObjectName(u"pushButtonGo")

        self.horizontalLayout.addWidget(self.pushButtonGo)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.webEngineView = QWebEngineView(Web)
        self.webEngineView.setObjectName(u"webEngineView")
        self.webEngineView.setUrl(QUrl(u"about:blank"))

        self.verticalLayout.addWidget(self.webEngineView)

        self.buttonBox = QDialogButtonBox(Web)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)

        self.verticalLayout.setStretch(1, 1)

        self.retranslateUi(Web)
        self.buttonBox.accepted.connect(Web.accept)
        self.buttonBox.rejected.connect(Web.reject)

        QMetaObject.connectSlotsByName(Web)
    # setupUi

    def retranslateUi(self, Web):
        Web.setWindowTitle(QCoreApplication.translate("Web", u"Dialog", None))
        self.pushButtonGo.setText(QCoreApplication.translate("Web", u"Go", None))
#if QT_CONFIG(shortcut)
        self.pushButtonGo.setShortcut(QCoreApplication.translate("Web", u"Return", None))
#endif // QT_CONFIG(shortcut)
    # retranslateUi

