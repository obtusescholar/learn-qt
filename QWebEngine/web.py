#! /usr/bin/env python3


import sys

from PySide2.QtWidgets import (
    QApplication,
    QDialog)
from PySide2.QtCore import QUrl
from PySide2.QtWebEngine import QtWebEngine

from . ui_web import Ui_Web


class Web(QDialog):
    __homepage = QUrl('https://www.qt.io/')
    __local = QUrl('http://localhost')

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Web()
        self.ui.setupUi(self)

        self.__openPage(self.__homepage)
        self.__buttons()

        self.show()

    def __openPage(self, url):
        self.ui.webEngineView.load(url)

    def __go(self):
        url = QUrl.fromUserInput(self.ui.lineEditUrl.text())
        self.__openPage(url)

    def __buttons(self):
        self.ui.pushButtonGo.clicked.connect(self.__go)


def main():
    app = QApplication([])
    widget = Web()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
